import React, { Component } from 'react';
import { 
  BrowserRouter as Router,
  Switch,
  Route, Redirect } from 'react-router-dom';
import Login from './pages/login'
import Main from './pages/main'

export default class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      database: [
        {
          "username": "admin",
          "password": "admin",
          "role":"admin"
        },
        {
          "username": "pedagang",
          "password": "pedagang",
          "role":"pedagang"
        },
        {
          "username": "user",
          "password": "user",
          "role":"user"
        }
      ],
      profile: 
      {
        "username": null,
        "password": null,
        "role": null,
        "status": false
      },
      barang: [
        {
          "namaBarang": "hp android",
          "harga": 10000000,
          "photo" : "andro1.jpg",
          "pedagang": "pedagang"
        }
      ]
    }
  }

  componentDidMount() {
    if (JSON.parse(localStorage.getItem("database")) == null) {
      localStorage.setItem("database", JSON.stringify(this.state.database))
      if (JSON.parse(localStorage.getItem("barang")) == null) {
        localStorage.setItem("barang", JSON.stringify(this.state.barang))
      }
    } 
  }

  login = (username, password, role) => {
    this.setState({ profile: 
    {
      "username": username,
      "password": password,
      "role": role,
      "status": true
    } })
  }

  logout = () => {
    this.setState({ profile: {
      "status": false
    }})
  }

  create = (username, password, role) => {
    this.state.database.push({
      "username": username,
      "password": password,
      "role": role
    })
    localStorage.setItem("database", JSON.stringify(this.state.database))
  }

  delete = (username) => {
    const findIndex = this.state.database.findIndex((value) => {
      return value.username === username
    })
    this.state.database.splice(findIndex,1)
    localStorage.setItem("database", JSON.stringify(this.state.database))
  }

  update = (cc ,username, password, role) => {
    const findIndex = this.state.database.findIndex((value) => {
      return value.username === cc
    })
    this.state.database.splice(findIndex,1, {
      "username" : username,
      "password" : password,
      "role" : role
    })
    localStorage.setItem("database", JSON.stringify(this.state.database))
  }

  createBarang = (namaBarang,harga,photo) => {
    this.state.barang.push({
      "namaBarang": namaBarang,
      "harga": harga,
      "photo": photo,
      "pedagang": this.state.profile.username
    })
    localStorage.setItem("barang", JSON.stringify(this.state.barang))
  }

  deleteBarang = (namaBarang) => {
    const findIndex = this.state.barang.findIndex((value) => {
      return value.namaBarang === namaBarang
    })
    this.state.barang.splice(findIndex,1)
    localStorage.setItem("barang", JSON.stringify(this.state.barang))
  }

  updateBarang = (cc, namaBarang,harga,photo) => {
    const findIndex = this.state.barang.findIndex((value) => {
      return value.namaBarang === cc
    })
    this.state.barang.splice(findIndex,1, {
      "namaBarang": namaBarang,
      "harga": harga,
      "photo": photo,
      "pedagang": this.state.profile.username
    })
    console.log(findIndex);
    localStorage.setItem("barang", JSON.stringify(this.state.barang))
  }

  render() {
    const loginOrMain = this.state.profile.status === false ? <Redirect to="/login"/> : <Redirect to="/"/>
    return (
      <Router>
        {loginOrMain}
        <Switch>
          <Route path="/login">
            <Login login={this.login} logout={this.logout}/>
          </Route>
          <Route path="/">
            <Main profile={this.state.profile} logout={this.logout} create={this.create} delete={this.delete} update={this.update} 
                  createBarang={this.createBarang} deleteBarang={this.deleteBarang} updateBarang={this.updateBarang}/>
          </Route>
        </Switch>
      </Router>
    )
  }
}
