import React, { Component } from 'react'
import './index.css'
import Admin from '../admin'
import Pedagang from '../pedagang'
import User from '../user'
import Navbar from '../component/navbar'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect
  } from "react-router-dom";

export default class Main extends Component {
    
    constructor(props)
    {
        super(props);
    }
    
    render() {
        const admin = this.props.profile.role === "admin" ? <Redirect to="/admin" /> : ""
        const pedagang = this.props.profile.role === "pedagang" ? <Redirect to="/pedagang" /> : ""
        const user = this.props.profile.role === "user" ? <Redirect to="/user" /> : ""
        return (
            <Router>
                <Navbar logout={this.props.logout}/>
                    {admin}
                    {pedagang}
                    {user}
                <Switch>
                    <Route path="/admin">
                        < Admin create={this.props.create} delete={this.props.delete} update={this.props.update}/>
                    </Route>
                    <Route path="/pedagang">
                        < Pedagang profile={this.props.profile} create={this.props.createBarang} delete={this.props.deleteBarang} update={this.props.updateBarang}/>
                    </Route>
                    <Route path="/user">
                        < User/>
                    </Route>
                    
                </Switch>
            </Router>
                
        )
    }
}
