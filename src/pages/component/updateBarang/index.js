import React, { Component } from 'react'
import './index.css'

export default class UpdateBarang extends Component {
    
    constructor(props)
    {
        super(props);
    }

    update = (e) => {
        e.preventDefault();

        const uBarang = e.target.updateBarang.value;
        const uHarga = e.target.updateHarga.value;
        const uPhoto = e.target.updatePhoto.value;

        this.props.update(this.props.cc,uBarang,uHarga,uPhoto);
        console.log(this.props.cc);
        this.props.status(false)
    }
    
    render() {
        return (
            <div>
                <br/>
                <div>Update Barang {}</div>
                <form onSubmit={this.update}>
                <input name="updateBarang" type="text" align="center" placeholder="Barang" ></input>
                <input name="updateHarga" type="number" align="center" placeholder="Harga"></input>
                <input name="updatePhoto" type="text" align="center" placeholder="Barang" ></input>
                <button type="submit" align="center">update</button>
                </form>
            </div>
        )
    }
}
