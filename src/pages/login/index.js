import React, { Component } from 'react'
import './index.css'

export default class Login extends Component {
    
    constructor(props)
    {
        super(props);
    }

    login = (e) => {
        e.preventDefault();

        const username = e.target.username.value;
        const password = e.target.password.value;

        const check = JSON.parse(localStorage.getItem("database")).filter(account => (account.username === username && account.password === password))
        if (check.length === 1) {
            this.props.login(check[0].username, check[0].password, check[0].role)
        }  else {
            alert ("Login Gagal!")
        }
    }
    
    render() {
        return (
            <div class="main">
                <p class="sign" align="center">Login</p>
                <form class="form" onSubmit={this.login}>
                    <input class="username" name="username" type="text" align="center" placeholder="Username" ></input>
                    <input class="password" name="password" type="password" align="center" placeholder="Password"></input>
                    <button type="submit" class="submit" align="center">Sign in</button>
                </form>
            </div>
        )
    }
}
