import React, { Component } from 'react'
import './index.css'
import Update from '../component/update'

export default class Admin extends Component {
    
    constructor(props)
    {
        super(props);
        this.state = {
            update: false,
            cc: null
        }
    }

    create = (e) => {
        e.preventDefault();

        const username = e.target.username.value;
        const password = e.target.password.value;
        const role = e.target.role.value;

        this.props.create(username,password,role);
        this.forceUpdate();
    }

    delete = (username) => {
        this.props.delete(username)
        this.forceUpdate()
    }

    update = (bool, cc) => {
        this.setState({
            update:bool,
            cc: cc
        })
    }

    render() {
        const list = JSON.parse(localStorage.getItem("database")).filter(account => account.role !== "admin").map((acc, index) =>
                <tr>
                    <td>{acc.username}</td>
                    <td>{acc.password}</td>
                    <td>{acc.role}</td>
                    <td><button onClick={() => this.update(true, acc.username)} >edit</button></td>
                    <td><button onClick={() => this.delete(acc.username)} >Delete</button></td>
                </tr>
                );
        const update = this.state.update === true ? <Update cc={this.state.cc} update={this.props.update} status={this.update}/> : ""
        return (
            <div>
                <table>
                    <thead>
                    <tr>
                        <th>Username</th>
                        <th>Password</th>
                        <th>Role</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                        {list}
                        </tbody>
                        
                </table>

                <br/>
                <div>Tambah data {}</div>
                <form onSubmit={this.create}>
                    <input class="usernamee" name="username" type="text" align="center" placeholder="Username" ></input>
                    <input class="passworde" name="password" type="password" align="center" placeholder="Password"></input>
                    <select name="role">
                        <option value="pedagang">Pedagang</option>
                        <option value="user">User</option>
                    </select>
                    <button type="submit" class="submite" align="center">tambah Akun</button>
                </form>
                <br/>
                <br/>
                {update}
            </div>
        )
    }
}
