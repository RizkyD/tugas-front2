import React, { Component } from 'react'
import './index.css'
import UpdateBarang from '../component/updateBarang'

export default class Pedagang extends Component {
    
    constructor(props)
    {
        super(props);
        this.state = {
            update: false,
            cc: null
        }
    }

    create = (e) => {
        e.preventDefault();

        const namaBarang = e.target.namaBarang.value;
        const harga = e.target.harga.value;
        const photo = e.target.photo.value;

        this.props.create(namaBarang,harga,photo);
        this.forceUpdate();
    }

    delete = (namaBarang) => {
        this.props.delete(namaBarang)
        this.forceUpdate()
    }

    update = (bool, cc) => {
        this.setState({
            update:bool,
            cc: cc
        })
    }

    render() {
        const list = JSON.parse(localStorage.getItem("barang")).filter(barang => barang.pedagang === this.props.profile.username).map((item) =>
                <tr>
                    <td>{item.namaBarang}</td>
                    <td>{item.harga}</td>
                    <td><img class="photo" alt={item.photo} src={item.photo}/></td>
                    <td><button onClick={() => this.update(true, item.namaBarang)} >edit</button></td>
                    <td><button onClick={() => this.delete(item.namaBarang)} >Delete</button></td>
                </tr>
                )
        const update = this.state.update === true ? <UpdateBarang cc={this.state.cc} update={this.props.update} status={this.update}/> : ""
        return (
            <div>
                <table>
                    <thead>
                    <tr>
                        <th>Barang</th>
                        <th>harga</th>
                        <th>photo</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                        {list}
                        </tbody>
                        
                </table>

                <br/>
                <div>Tambah Barang {}</div>
                <form onSubmit={this.create}>
                    <input class="barange" name="namaBarang" type="text" align="center" placeholder="Nama Barang" ></input>
                    <input class="hargae" name="harga" type="number" align="center" placeholder="harga"></input>
                    <input class="photoe" name="photo" type="text" align="center" placeholder="photo/url" ></input>
                    <button type="submit" class="submite" align="center">tambah Barang</button>
                </form>
                <br/>
                <br/>
                {update}
            </div>
        )
    }
}
