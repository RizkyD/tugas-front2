import React, { Component } from 'react'
import './index.css'

export default class User extends Component {
    
    constructor(props)
    {
        super(props);
        this.state = {
            update: false,
            cc: null,
            cart: []
        }
    }

    addcart = (cc) => {
        let json = JSON.parse(localStorage.getItem("barang")).filter(item => item.namaBarang === cc)
        const name = this.state.cart.find((value) => {
            return value.namaBarang === cc
          })
          if (name === undefined) {
            this.setState({
                cart: [...this.state.cart, ...json]
            })
          } else {
            alert("Data udah di cart")
          }
        
        this.forceUpdate()
    }

    deleteCart = (cc) => {
        const findIndex = this.state.cart.findIndex((value) => {
            return value.namaBarang === cc
          })
        this.state.cart.splice(findIndex,1)
        this.forceUpdate()
    }

    render() {
        const list = JSON.parse(localStorage.getItem("barang")).map((item) =>
                <tr>
                    <td>{item.namaBarang}</td>
                    <td>{item.harga}</td>
                    <td><img class="photo" alt={item.photo} src={item.photo}/></td>
                    <td><button onClick={() => this.addcart(item.namaBarang)} >add cart</button></td>
                </tr>
                )
        const cart = this.state.cart.map((item) =>
                <tr>
                    <td>{item.namaBarang}</td>
                    <td>{item.harga}</td>
                    <td><img class="photo" alt={item.photo} src={item.photo}/></td>
                    <td><button onClick={() => this.deleteCart(item.namaBarang)} >delete cart</button></td>
                </tr>
                )
        return (
            <div class="exceed">
                <div>
                <h3>data barang</h3>
                <table>
                    <thead>
                    <tr>
                        <th>Barang</th>
                        <th>harga</th>
                        <th>photo</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                        {list}
                        </tbody>
                        
                </table>
                </div>
                <div>
                <h3>data Cart</h3>
                <table>
                    <thead>
                    <tr>
                        <th>Barang</th>
                        <th>harga</th>
                        <th>photo</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                        {cart}
                        </tbody>
                        
                </table>
                </div>
            </div>
        )
    }
}
